const express = require('express')
const app = express()
const hbs = require('hbs');

//asignación de puerto dinamico por Heroku
const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

//Express hbs engine
hbs.registerPartials(__dirname + '/views/parciales');
app.set('view engine', 'hbs');

app.get('/', (req, res) => {
    res.render('home', {
        nombre: 'Fernando',
        anio: new Date().getFullYear()
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        nombre: 'Angry Red!',
        desc: 'Es el pájaro rojo enojón!!!'
    });
});

app.listen(port, () => console.log(`escuchando peticiones en el puerto ${port}`));