const http = require('http');

http.createServer((req, res) => {
        console.log("Escuchando el puerto 8080");
        res.writeHead(200, { 'Content-Type': 'application/json' });
        let salida = {
            nombre: 'Fernando',
            edad: 28,
            url: req.url
        };
        res.write(JSON.stringify(salida));
        //res.write("Hola Mundo");
        res.end();
    })
    .listen(8080);